Interacting with VPFIT
======================

Extracting fitting model
------------------------

.. currentmodule:: velplot.fit.fortread

.. autosummary::
   :toctree: generated/

   vpfit_output
   read_fort13
   read_fort26
   get_dv
   get_trans
   check_col_dens

Storing atomic data
-------------------
   
.. currentmodule:: velplot.fit.atominfo

.. autosummary::
   :toctree: generated/
   
   isfloat
   atom_list
   atom_info
   atom_mass

Executing VPFIT
---------------
   
.. currentmodule:: velplot.fit.run

.. autosummary::
   :toctree: generated/

   get_data
   make_setup
   create_chunks
   fit_fort
   
