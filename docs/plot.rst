Velocity plotting
=================

Main function
-------------

.. currentmodule:: velplot.main

.. autosummary::
   :toctree: generated/

   make_figure

Standard still figure
---------------------

.. currentmodule:: velplot.plot.standard

.. autosummary::
   :toctree: generated/

   make_pdf
   custom_plot
   get_index
   setup_fig
   plot_region_plt
   add_text
   
interactive figure
------------------

.. currentmodule:: velplot.plot.interactive

.. autosummary::
   :toctree: generated/

   interplot
   trace_for_legend
   get_custom_data
   plotly_region
   make_titles
   
