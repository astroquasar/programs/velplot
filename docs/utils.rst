Other Utilities
===============

Configuration settings
----------------------

.. currentmodule:: velplot.config

.. autosummary::
   :toctree: generated/

   default_settings
   load_config
   merge

Utilities
---------
   
.. currentmodule:: velplot.utils

.. autosummary::
   :toctree: generated/
   
   get_ion
   add_line
   add_head
   add_hydro
   
