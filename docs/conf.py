project      = "velplot"
base_url     = "https://astroquasar.gitlab.io/programs/velplot/"
repo_url     = "https://gitlab.com/astroquasar/programs/velplot/"
repo_name    = "astroquasar/velplot"
tutorial     = "https://colab.research.google.com/drive/1DOJCeCTnt1eNo1afn5nEgoAcdr_eO2lu"
description  = "Plotting program of quasar absorption line systems in velocity scale with individual Voigt profile component."
color        = "deep-orange" #ff7043
logo         = '<i class="fab fa-cloudversify"></i>'
html_favicon = "_static/favicon.ico"

import os,sys,sphinx_material
sys.path.insert(0, os.path.abspath('../%s'%project))
sys.path.insert(0, os.path.abspath('../bin'))
from distutils.version import LooseVersion
from recommonmark.transform import AutoStructify

FORCE_CLASSIC = os.environ.get("SPHINX_MATERIAL_FORCE_CLASSIC", False)
FORCE_CLASSIC = FORCE_CLASSIC in ("1", "true")

html_title = "%s Documentation"%project.upper()
copyright = "2020, Vincent Dumont"
author = "Vincent Dumont"
release = LooseVersion(sphinx_material.__version__).vstring

extensions = [
    "sphinx.ext.autodoc",
    'sphinx.ext.autosummary',
    "numpydoc",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "nbsphinx",
    "recommonmark",
    "sphinx_markdown_tables",
    "sphinx_copybutton",
    "IPython.sphinxext.ipython_console_highlighting",
    "sphinx_material"
]

language = "en"
todo_include_todos = True
nbsphinx_execute = "always"
nbsphinx_kernel_name = "python3"
templates_path = ["_templates"]
autosummary_generate = True
autoclass_content = "class"
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", "**.ipynb_checkpoints"]

html_last_updated_fmt = ""
html_use_index = True
html_domain_indices = True
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = { "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"] }
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"
html_css_files = ['https://use.fontawesome.com/releases/v5.10.2/css/all.css',
                  'justify_text.css','pygments.css','video_responsive.css','component.css','gwpy-sphinx.css']
html_theme_options = {
    "base_url": base_url,
    "repo_url": repo_url,
    "repo_name": repo_name,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "logo_icon": logo,
    "repo_type": "gitlab",
    "globaltoc_depth": 1,
    "color_primary": color,
    "color_accent": color,
    "touch_icon": "_static/ic_blur_on_48px-512.png",
    "master_doc": False,
    "nav_links": [
        {"href": "index", "internal": True, "title": "Documentation"},
        {
            "href": tutorial,
            "internal": False,
            "title": "Tutorial on Google Colab",
        },
    ],
    "heroes": {
        "index": description,
    },
    "table_classes": ["plain"],
}

if FORCE_CLASSIC:
    print("!!!!!!!!! Forcing classic !!!!!!!!!!!")
    html_theme = "classic"
    html_theme_options = {}
    html_sidebars = {"**": ["globaltoc.html", "localtoc.html", "searchbox.html"]}

extlinks = { "duref": ("http://docutils.sourceforge.net/docs/ref/rst/" "restructuredtext.html#%s",""),
             "durole": ("http://docutils.sourceforge.net/docs/ref/rst/" "roles.html#%s", ""),
             "dudir": ("http://docutils.sourceforge.net/docs/ref/rst/" "directives.html#%s", ""),
            }

def setup(app):
    app.add_config_value(
        "recommonmark_config",
        {"enable_math": True, "enable_inline_math": True, "enable_eval_rst": True},
        True,
    )
    app.add_transform(AutoStructify)
    app.add_object_type(
        "confval",
        "confval",
        objname="configuration value",
        indextemplate="pair: %s; configuration value",
    )
    
htmlhelp_basename = '%sdoc'%project.upper()
intersphinx_mapping = {'https://docs.python.org/': None,
                       'numpy': ('https://numpy.org/doc/stable/', None),
                       'torch': ('https://pytorch.org/docs/stable/', None)}
numpydoc_show_class_members = False
nbsphinx_allow_errors = True
nbsphinx_execute = 'never'
autosummary_generate = True
autodoc_default_flags = ['members']
