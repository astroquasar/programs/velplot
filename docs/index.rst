.. title:: Docs

.. image:: _images/logo.png
   :class: no-scaled-link
   :width: 40%
	   
.. raw:: html

   <br>

VELPLOT is a software package that can be used to display quasar absorption line systems in velocity scale. The systems can be displayed either in the form of a still or interactive figure. The program was built by `Vincent Dumont <https://vincentdumont.gitlab.io/>`_ and is part of the `Astroquasar <https://astroquasar.gitlab.io/>`_ group's toolset available in our `GitLab group <https://gitlab.com/astroquasar/programs/velplot>`_.

.. image:: _images/example.png

Installation
------------

The easiest way to install the software is using the `PyPI package manager <https://pypi.org/project/velplot/>`_ as follows:
 
.. code-block:: none

   pip install velplot

The above command will install all the required dependencies (see list below) along with the main software. More information about the package can be found in the `pypi webpage
<https://pypi.org/project/hpo-uq/>`_.

.. literalinclude:: ../requirements.txt
   :language: html

License Agreement
-----------------

.. literalinclude:: ../LICENSE.txt
   :language: html

Reporting issues
----------------

.. raw:: html

   <br>
   <a href="https://gitlab.com/astroquasar/programs/velplot/-/issues/new?issue%5Bmilestone_id%5D=" class="button3">
   <font color="#ffffff">Submit a ticket</font>
   </a>

If you find any bugs when running this program, please make sure to report them by clicking on the above link and
submit a ticket on the software's official Gitlab repository.

.. toctree::
   :caption: Getting Started
   :hidden:

   tutorial.ipynb

.. toctree::
   :caption: Python Modules
   :hidden:

   fit
   plot
   utils
   
