# A plotting routine for quasar absorbers

[![License MIT](https://img.shields.io/badge/License-MIT-blue.svg)](http://opensource.org/licenses/MIT)
[![PyPI version](https://badge.fury.io/py/velplot.svg)](https://badge.fury.io/py/velplot)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.437913.svg)](https://doi.org/10.5281/zenodo.437913)

## Description

This code allows you to do velocity plots from fort.13 or fort.26 models.

## Installation

To install, do the following:

1. Add the absolute path to the `velplot/` directory to your `PYTHONPATH`.
2. Add the absolute path to the `velplot/scripts` directory to your system `PATH` variable

In order to do so, please edit your local .profile (or .bashrc, or similar) and insert the following lines:

```bash
export PYTHONPATH=$PYTHONPATH:/path/to/velplot/
export PATH=$PATH:/path/to/velplot/scripts/
```

You will then be able to run the program from wherever you want on your computer.

## Usage

The usage is straightforward and correspond to the following:

```bash
velplot <fortfile> [--args]
```

For example, you can do the following:

```bash
velplot fort.13 --output turbulent --version vpfit10 --header header.dat
```

## Arguments

Here are all the possible options:

| Argument       | Description                                   |
| -------------- | --------------------------------------------- |
| `--atomdir`    | Path to a custom atom.dat                     |
| `--details`    | Plot individual Voigt profiles                |
| `--dispersion` | Give custom velocity dispersion               |
| `--getwave`    | Get wavelength array from the spectrum        |
| `--header`     | List of transition IDs of each fitting region |
| `--nores`      | Do not plot the residuals                     |
| `--output`     | Give custom output filename                   |
| `--version`    | Path to a custom vpfit executable             |
| `--vpfsetup`   | Path to a custom vp_setup.dat                 |

By default, the program assumes the following:

(1) Both atom.dat and `vp_setup.dat` are located in the same repository where the `for.13` and/or `fort.26` are. Options `--atomdir` and `--vpfsetup` can be used to provide the paths to custom `atom.dat` and `vp_setup.dat` files.

(2) The default VPFIT command is just `vpfit` and is therefore assumed to be included in the PATH.
    The option `--version` can be used to provide another VPFIT version.

(3) The high dispersion individual Voigt profiles are not plotted by default, you need to use the
    option `--details` to plot the profiles. If not used, the program will only display the model
    from VPFIT taken from the chunks.

(4) The velocity dispersion for the high dispersion Voigt profiles generated is 0.01 km/s.
    Option `--dispersion` can be considered to give a custom velocity dispersion.

## Transition list

Finally, one needs to provide the list transition IDs of each fitting region as this is the only
way the program can align the fitting regions in velocity scale. This can be done by including the
IDs inside the fort.13 header, or in a separate header.dat file. The program does not look for IDs
in the fort.26. The IDs should be written as followed:

```bash
[species][ionization level]_[approximate wavelength]
```

For example, a transition ID can be something like: OI\_1320, FeII\_1608, OI\_988.77. The wavelength does not have to be accurate, the program will take the closest transition for that species and that ionisation level. If the absorption system contain fitting regions of another system that blends the system that is fitted, one needs to mention it in the list of IDs so that the program knows how to deal with the velocity alignment, and can display a little comment about it as you can see in the figure in the =example.pdf= file. In this example the complete list of headers is:

```bash
MgII_2796
MgII_2803
MgI_2852
FeII_2600
MgII_2796 external MgII_2803
FeII_2586 overlap  MgII_2803
FeII_2600 external MgII_2803
```

The fitting region with the =overlap= comment in the ID list correspond to the region blended by the MgII_2803 transition of the external absorption system for which 2 external regions are also displayed.

## License

(The MIT License)

Copyright (c) 2016-2017 Vincent Dumont

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
