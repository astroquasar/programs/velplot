import numpy

def get_ion(line):
  line = line.strip().split()
  name = None if len(line)<=1 else line[0] if len(line[0])>1 else line[0]+line[1]
  return name

def add_line(line,icomp,anchor=False,switch=None,tie=False,ratio=1):
  # Remove edge spaces and split line by space delimiter
  chars = line.strip().split()
  # Get column density value
  l = len(chars[0])
  col_dens = chars[1] if l>1 else chars[2]
  # Find out index range where ion name is placed
  imin = line.find(chars[0])
  imax = line.find(col_dens)
  # Replace ion name if requested
  if switch!=None:
    ion_name = switch+' '*(imax-imin-len(switch))
    line = line[:imin]+ion_name+line[imax:].upper()
  # Replace column density based on ratio value
  idx = imax-2
  d2h = -4.6 if switch=='DI' else 0
  N_new = '%.5f'%(ratio*float(col_dens)+d2h)
  N_new = ('{:>%s}'%(imax+len(col_dens)-idx)).format(N_new)
  line = line[:idx]+N_new+line[imax+len(col_dens):].upper()
  # Loop over every character and add tie parameter after column density
  new_line = ''
  for i,char in enumerate(line):
    if tie and i==imax+len(col_dens):
      char = '%' if icomp==0 else 'x' if anchor else 'X'
    new_line+=char
  return(new_line)

def add_head(line,params):
  vals = line.strip().split()
  imin = line.find(vals[1])+2
  imax = line.find(vals[4])
  iend = -1 if len(vals)<=5 else line.find(vals[5])
  new_line = ''
  for ion,wmin,wmax in params:
    new_line += '{}{:>11.3f}{:>11.3f}   {}!{}\n'.format(line[:imin],wmin,wmax,line[imax:iend],ion)
  return new_line

def add_hydro(fort13,anchor,dest=None,tie=False,header=[],ratio=1,flush=False):
  """
  This function
  """
  old_fort = numpy.loadtxt(fort13,delimiter='\n',dtype=str)
  if dest==None:
    dest = fort13.replace('.13','_new.13')
  new_fort = open(dest,'w')
  star, icomp, comps = 0, 0, []
  for i,line in enumerate(old_fort):
    if line.strip()=='':
      break
    elif '*' in line:
      star += 1
      if star==2 and header!=[]:
        head_line = add_head(old_fort[i-1],header)
        new_fort.write(head_line)
      new_fort.write(line+'\n')
    elif star==1 and flush==False:
      new_fort.write(line+'\n')
    elif star==2:
      this_ion = get_ion(old_fort[i])
      next_ion = get_ion(old_fort[i+1])
      if this_ion==anchor:
        comps.append(line)
        line = add_line(line,icomp,anchor=True,tie=tie)
        icomp += 1
      new_fort.write(line+'\n')
      if this_ion==anchor and next_ion!=this_ion:
        for j,anchor_line in enumerate(comps):
          line = add_line(anchor_line,j,switch='HI',tie=tie,ratio=ratio)
          new_fort.write(line+'\n')
        for j,anchor_line in enumerate(comps):
          line = add_line(anchor_line,j,switch='DI',tie=tie,ratio=ratio)
          new_fort.write(line+'\n')
  new_fort.close()
