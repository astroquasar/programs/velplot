# Local
import os

# External
import numpy
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def make_pdf(info,data,settings,**kwargs):
    print('Creating figure...')
    setup_fig(info['seaborn'],info['pubstyle'])
    pdf_pages = PdfPages(info['current_dir']+'/'+info['output']+'.pdf')
    i=0
    while (i<len(info['table1'])):
        f   = 1
        ref = i + 6          # position in table1 to define where to create new plotting page
        fig = plt.figure(figsize=(8.27, 11.69))
        plt.axis('off')
        plt.subplots_adjust(left=0.1, right=0.9, bottom=0.06, top=0.96, wspace=0, hspace=0)
        while i<len(info['table1']) and i<ref:
            ax = fig.add_subplot(6,1,f,xlim=[info['dvmin'],info['dvmax']],ylim=[-0.6,2.1])
            ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
            if info['extra']!=None:
                for extrav in info['extra'].split(':'):
                    ax.axvline(float(extrav),color='0.5',ls='dotted',lw=0.8)
            ax.text(info['dvmin'],1.6,r'$-1\sigma$ ',ha='right',va='center',size=8)
            ax.text(info['dvmin'],1.8,r'$+1\sigma$ ',ha='right',va='center',size=8)
            if f==1: plt.title(os.path.abspath(info['fortfile']),fontsize=7)
            if i+1!=ref and i+1!=len(info['table1']): plt.setp(ax.get_xticklabels(), visible=False)
            else: ax.set_xlabel('Velocity relative to $z_{abs}=%.6f$ (km/s)'%info['zmid'],fontsize=10)
            plot_region_plt(i,settings,info,data)
            i = i + 1
            f = f + 1
        pdf_pages.savefig(fig)
    pdf_pages.close()

def custom_plot(info,data,settings,**kwargs):
    print('Creating figure...')
    setup_fig(info['seaborn'],info['pubstyle'])
    ncols, nrows = info['ncols'], info['nrows']
    if ncols!=None:
        fraction = len(info['table1'])/ncols
        nrows = int(fraction+1) if float(fraction)/int(fraction)>1 else int(fraction)
        figsize = [ncols*4,nrows*2]
        # figsize = [8.27,11.69/6*nrows]
    elif nrows!=None:
        fraction = len(info['table1'])/nrows
        ncols = int(fraction+1) if float(fraction)/int(fraction)>1 else int(fraction)
        figsize = [ncols*4,nrows*2]
        # figsize = [8.27,11.69/6*nrows]
    else:
        figsize = [8.27,2*len(info['table1'])]
        nrows = len(info['table1'])
        ncols = 1
    figsize = [size*info['display_ratio']/100. for size in figsize]
    fig = plt.figure(figsize=figsize,frameon=False,dpi=200)
    plt.subplots_adjust(left=0.05,right=0.97,bottom=0.05,top=0.98,wspace=0.04,hspace=0.07)
    posidx = get_index(len(info['table1']),nrows,ncols,info['posidx'])
    for i,idx in enumerate(posidx):
        ax = fig.add_subplot(nrows,ncols,idx,xlim=[info['dvmin'],info['dvmax']],ylim=settings['ylimit'])
        # Plot transition name
        if info['pubstyle']:
            plt.text(0.5,0.01,info['header'][i,0]+' %.2f'%float(info['header'][i,1]),
                     transform=ax.transAxes,fontsize=settings['label']['size'],color='black',ha='center',va='bottom')
        if info['extra']!=None:
            for extrav in info['extra'].split(':'):
                ax.axvline(float(extrav),color='0.5',ls='dotted',lw=0.8)
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        if (idx-1)%ncols==0:
            if info['pubstyle']==False:
                ax.text(info['dvmin'],1.6,r'$-1\sigma$  ',ha='right',va='center',size=8)
                ax.text(info['dvmin'],1.8,r'$+1\sigma$  ',ha='right',va='center',size=8)
        else:
            plt.setp(ax.get_yticklabels(), visible=False)
        if idx-1<len(info['table1'])-ncols:
            plt.setp(ax.get_xticklabels(), visible=False)
        plot_region_plt(i,settings,info,data)
    fig.text(0.5, 0.01, 'Velocity relative to $z_{abs}=%.6f$ (km/s)'%info['zmid'], ha='center',fontsize=10)
    # plt.tight_layout()
    if info['pubstyle']:
        fig.text(0, 0.5, 'Normalized Flux', va='center', rotation='vertical',fontsize=10)
    if info['save_to_file']:
        plt.savefig(info['current_dir']+'/'+info['output']+'.pdf')
    else:
        plt.show()

def get_index(n,nrows,ncols,posidx):
    # Assign region index to target subplot array location
    i = 0
    pos = numpy.arange(nrows*ncols)
    pos[:len(posidx)] = [i-1 for i in posidx]
    idxs = numpy.zeros((nrows,ncols),dtype=int)
    for col in range(ncols):
        for row in range(nrows):
            idxs[row,col] = pos[i]+1
            i+=1
    # Cross-match region index with actual subplot index
    return idxs.flatten()[:n].argsort()+1
    
def setup_fig(seaborn,pubstyle):
    # Use seaborn style if requested
    if seaborn:
        plt.style.use('seaborn')
    # Select standard or publishing style
    if pubstyle:
        plt.rc('font',   size=10, family='sans-serif')
        plt.rc('axes',   labelsize=10, linewidth=0.2)
        plt.rc('legend', fontsize=10, handlelength=5)
        plt.rc('xtick',  labelsize=8)
        plt.rc('ytick',  labelsize=8)
        plt.rc('lines',  lw=0.2, mew=0.2)
    else:
        plt.rc('font', size=2, family='serif')
        plt.rc('axes', labelsize=2, linewidth=0.2)
        plt.rc('legend', fontsize=2, handlelength=10)
        plt.rc('xtick', labelsize=8)
        plt.rc('ytick', labelsize=8)
        plt.rc('lines', lw=0.2, mew=0.2)
        plt.rc('grid', linewidth=0.2)
        
def plot_region_plt(i,settings,info,data):
    colors = settings['colors']
    if info['pubstyle']==False:
        add_text(i,data,**info)
    # Plot residuals
    if 'res' in data[i].keys():
        plt.axhline(y=1.6,color=settings['res']['color'],zorder=2,lw=0.3)
        plt.axhline(y=1.7,color=settings['res']['color'],ls='dotted',zorder=2,lw=0.3)
        plt.axhline(y=1.8,color=settings['res']['color'],zorder=2,lw=0.3)
        plt.plot(*data[i]['res'].values(),**settings['res'],zorder=3)
    # Plot model from VPFIT
    if 'mod' in data[i].keys():
        plt.plot(*data[i]['mod'].values(),**settings['mod'],zorder=1)
        if settings['fill']:
            plt.fill_between(*data[i]['mod'].values(),1,color=settings['mod']['color'],lw=0,alpha=0.2)
        # Plot error array
        if info['pubstyle']==False:
            plt.plot(*data[i]['err'].values(),**settings['err'])
    # Plot data
    plt.plot(*data[i]['dat'].values(),**settings['dat'],zorder=2)
    # Plot individual profiles
    if info['details']:
        for n in data[i]['cmp'].keys():
            plt.plot(*data[i]['cmp'][n]['data'].values(),**settings['cmp'])
    # Plot vertical line for every component
    for n in data[i]['cmp'].keys():
        #header, atom = info['header'], info['atom']
        vobs = data[i]['cmp'][n]['vobs']
        if info['pubstyle']:
            if info['details'] or (data[i]['mod']['x'][0]<=vobs<=data[i]['mod']['x'][-1]):
                plt.plot([vobs,vobs],[1.1,1.2],color='red',lw=0.5)
        else:
            z = data[i]['cmp'][n]['z']
            pos = 1.08 if data[i]['cmp'][n]['val']%2==0 else 1.25
            category =  data[i]['cmp'][n]['category']
            plt.axvline(x=vobs,ls='dashed',color=colors[category],lw=.5,zorder=1,alpha=0.7)
            if 'label' in data[i]['cmp'][n].keys():
                label =  data[i]['cmp'][n]['label']
                t = plt.text(vobs,pos,label,color=colors[category],weight='bold',fontsize=7,horizontalalignment='center')
                t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
    # Plot high-resolution co-added profile
    if info['details']:
        plt.plot(*data[i]['prf'].values(),**settings['prf'])        
    plt.axhline(y=1,color='black',ls='dotted',lw=0.3)
    plt.axhline(y=0,color='black',ls='dotted',lw=0.3)
        
def add_text(i, data, header, dvmin, dvmax, table1, comment, **kwargs):
    vcent = (dvmin+dvmax)/2
    vhalf = (dvmax-dvmin)/2
    if data[i]['shift']!=0:
        t = plt.text(vcent-.5*vhalf,-.43,'>> %.5f km/s'%(-data[i]['shift']),color='blue',fontsize=5)
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
    if data[i]['cont']!=0:
        t = plt.text(vcent-.5*vhalf,-.28,'<> %.5f'%data[i]['cont'],color='blue',fontsize=5)
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
    if data[i]['zero']!=0:
        t = plt.text(vcent-.27*vhalf,-.28,'__ %.5f'%(-data[i]['zero']),color='blue',fontsize=5)
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))    
    t1 = plt.text(vcent-.97*vhalf,-.4,
                  str(header[i,0])+' '+str('%.2f'%float(header[i,1])),
                  color='blue',fontsize=10,ha='left')
    t2 = plt.text(vcent-.1*vhalf,-.28,
                  ' f = %.4f'%float(header[i,2]),
                  color='blue',fontsize=5,ha='left')
    t3 = plt.text(vcent+.1*vhalf,-.28,
                  ' $\chi^2_{\mathrm{abs}}$ = '+header[i,-4],
                  color='blue',fontsize=5,ha='left')
    t5 = plt.text(vcent+.1*vhalf,-.43,
                  'npix = '+header[i,-2],
                  color='blue',fontsize=5,ha='left')
    t4 = plt.text(vcent+.35*vhalf,-.28,
                  ' $\chi^2_{\mathrm{red}}$ = '+header[i,-3],
                  color='blue',fontsize=5,ha='left')
    t6 = plt.text(vcent+.35*vhalf,-.43,
                  'ndf  = '+header[i,-1],
                  color='blue',fontsize=5,ha='left')
    t7 = plt.text(vcent+.97*vhalf,-.4,
                  str(i+1)+' - '+str(table1[i][0].split('/')[-1]),
                  color='blue',fontsize=7,ha='right')
    for t in [t1,t2,t3,t4,t5,t6,t7]:
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))

    if header[i,4]!='0':
        t = plt.text(vcent-.1*vhalf,-.43,
                     'q = %.0f'%float(header[i,4]),
                     color='blue',fontsize=5,ha='left')
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))        
    if 'overlap' in comment[i,0]:
        t = plt.text(vcent+.97*vhalf,.3,
                     'Overlapping system:\n'+comment[i,1],
                     color='darkorange',weight='bold',fontsize=6,horizontalalignment='right')
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))        
    if 'external' in comment[i,0]:
        t = plt.text(vcent+.97*vhalf,.3,
                     'External system:\n'+comment[i,1],
                     color='darkorange',weight='bold',fontsize=6,horizontalalignment='right')
        t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))

