# External
import numpy
import plotly.graph_objects as go
from plotly.subplots import make_subplots

# Internal
from ..constants import c

def interplot(info,data,settings,**kwargs):
    print('Creating figure...')
    # Extract settings
    height = 400*len(info['table1'])
    # Initialize list for legend values
    comps = []
    # Initialize subplots
    fig = make_subplots(rows=len(info['table1']),cols=1,shared_xaxes=True,shared_yaxes=True,
                        horizontal_spacing=0.05,vertical_spacing=50./height,
                        subplot_titles=make_titles(data,height,**info))
    # Make hidden plot to plot legend in order from fort.13 file
    fig = trace_for_legend(fig,data)
    # Loop through each region
    for i in range(len(info['table1'])):
        # Horizontal lines for error array
        fig.add_shape(go.layout.Shape(type="line",x0=info['dvmin'],x1=info['dvmax'],y0=1.6,y1=1.6),
                                      line=dict(color=settings['res']['line']['color'],width=0.8),
                      row=i+1, col=1)
        fig.add_shape(go.layout.Shape(type="line",x0=info['dvmin'],x1=info['dvmax'],y0=1.7,y1=1.7,
                                      line=dict(color=settings['res']['line']['color'],width=0.8,dash='dot')),
                      row=i+1, col=1)
        fig.add_shape(go.layout.Shape(type="line",x0=info['dvmin'],x1=info['dvmax'],y0=1.8,y1=1.8,
                                      line=dict(color=settings['res']['line']['color'], width=0.8)),
                      row=i+1, col=1)
        # Horizontal lines for zero level and continuum
        fig.add_shape(go.layout.Shape(type="line",x0=info['dvmin'],x1=info['dvmax'],y0=0,y1=0),
                                      line=dict(color='black', width=0.8,dash='dot'),
                      row=i+1, col=1)
        fig.add_shape(go.layout.Shape(type="line",x0=info['dvmin'],x1=info['dvmax'],y0=1,y1=1),
                                      line=dict(color='black', width=0.8,dash='dot'),
                      row=i+1, col=1)
        # Fill each region
        fig, comps = plotly_region(i,fig,settings,info,data,comps)
    # Ensure plotting of xtick labels for all subplots
    showticklabels = {}
    for key in  ['xaxis%s_showticklabels'%('' if i+1==1 else i+1) for i in range(len(info['table1']))]:
        showticklabels[key] = True
    fig.update_layout(**showticklabels)
    # Align all subplot title to left
    for i in range(len(info['table1'])):
        fig.layout.annotations[i].update(x=0,xanchor='left')
    # Update height and figure title
    fig.update_xaxes(range=[info['dvmin'],info['dvmax']])
    fig.update_yaxes(range=[-0.6,2.1],tickvals=[0,1,1.6,1.8],ticktext=[0,1,'-1&#963;','+1&#963;'])
    fig.update_layout(title_text='Velocity relative to z<sub>abs</sub>=%.6f (km/s)'%info['zmid'],height=height)
    fig.update_layout(legend=dict(yanchor="top",y=1,xanchor="left",x=-0.12),autosize=True)
    if info['save_to_file']:
        fig.write_html(info['current_dir']+'/'+info['output']+'.html')
    else:
        fig.show()

def trace_for_legend(fig,data):
    info = []
    for i in data.keys():
        for n in data[i]['cmp'].keys():
            info.append([data[i]['cmp'][n]['pos'],n,i])
    info = numpy.array(info)
    info = info[numpy.unique(info[:,0],axis=0,return_index=True)[1]]
    for pos,n,i in info:
        tie = data[i]['cmp'][n]['tie']
        label = tie if tie=='' else 'Tie parameter: %s<br>'%tie
        name = '<b>%03i</b> - %s %s'%(pos,data[i]['cmp'][n]['ion'],tie)
        fig.add_trace(go.Scatter(x=[0],y=[0],opacity=0,mode='markers',name=name,
                                 legendgroup=name,marker={'size':0,'color':'black'}
    ))
    return fig
        
def get_custom_data(zmid,lamb,x,y):
    data_in_red = float(zmid) * (2*c+x) / (2*c-x) + 2*x / (2*c-x)
    data_in_wav = float(lamb) * (data_in_red+1)
    return numpy.array([data_in_wav,data_in_red]).T

def plotly_region(i,fig,settings,info,data,comps):
    colors = settings['colors']
    text_coord = \
        'F = %{y:.5f}<br>' + \
        'v = %{x:.2f} m/s<br>' + \
        '&#955; = %{customdata[0]:.4f} &#8491; <br>' + \
        'z = %{customdata[1]:.7f}<br>' + \
        '<extra></extra>'
    # Plot residuals
    if 'res' in data[i].keys():
        fig.add_trace(go.Scatter(**data[i]['res'],**settings['res']['feat'],line=dict(**settings['res']['line']),
                                 customdata=get_custom_data(info['zmid'],data[i]['wav'],**data[i]['res']),
                                 hovertemplate='<b>Residual</b><br>'+text_coord,
                                 mode='lines',showlegend=False),
                      row=i+1, col=1)
    # Plot model from VPFIT
    if 'mod' in data[i].keys():
        fig.add_trace(go.Scatter(**data[i]['mod'],**settings['mod']['feat'],line=dict(**settings['mod']['line']),
                                 customdata=get_custom_data(info['zmid'],data[i]['wav'],**data[i]['mod']),
                                 hovertemplate='<b>VPFIT output model</b><br>'+text_coord,
                                 mode='lines',showlegend=False),
                      row=i+1, col=1)
        # Plot error array
        fig.add_trace(go.Scatter(**data[i]['err'],**settings['err']['feat'],line=dict(**settings['err']['line']),
                                 customdata=get_custom_data(info['zmid'],data[i]['wav'],**data[i]['err']),
                                 hovertemplate='<b>Error</b><br>'+text_coord,
                                 mode='lines',showlegend=False),
                      row=i+1, col=1)
    # Plot data
    fig.add_trace(go.Scatter(**data[i]['dat'],**settings['dat']['feat'],line=dict(**settings['dat']['line']), 
                             customdata=get_custom_data(info['zmid'],data[i]['wav'],**data[i]['dat']),
                             hovertemplate='<b>%s %s</b><br>'%(info['header'][i,0],info['header'][i,1])+text_coord,
                             mode='lines',showlegend=False),
                  row=i+1, col=1)
    # Plot individual profiles
    if info['details']:
        for n in data[i]['cmp'].keys():
            tie = data[i]['cmp'][n]['tie']
            label = tie if tie=='' else 'Tie parameter: %s<br>'%tie
            name = '<b>%03i</b> - %s %s'%(data[i]['cmp'][n]['pos'],data[i]['cmp'][n]['ion'],tie)
            settings['cmp']['line']['color'] = colors[data[i]['cmp'][n]['category']]
            fig.add_trace(go.Scatter(**data[i]['cmp'][n]['data'],**settings['cmp']['feat'],line=dict(**settings['cmp']['line']),
                                     customdata=get_custom_data(info['zmid'],data[i]['cmp'][n]['wav'],**data[i]['cmp'][n]['data']),
                                     hovertemplate= \
                                     '<b>%s %.2f</b><br>'%(data[i]['cmp'][n]['ion'],float(data[i]['cmp'][n]['wav'])) + \
                                     '&#955; = %.4f &#8491; <br>'%float(data[i]['cmp'][n]['wav']) + \
                                     'f = %.6f<br>'%float(data[i]['cmp'][n]['osc']) + \
                                     '&#915; = %s<br>'%data[i]['cmp'][n]['gam'] + \
                                     '<br><b>Component n°%i</b><br>'%data[i]['cmp'][n]['pos'] +
                                     'z<sub>cent</sub> = %.7f<br>'%data[i]['cmp'][n]['z'] +
                                     'log(N) = %.5f<br>'%data[i]['cmp'][n]['N'] +
                                     'b = %.4f m/s<br>'%data[i]['cmp'][n]['b'] + label +
                                     '<br><b>Coordinates</b><br>' + text_coord,
                                     mode='lines',name=name,legendgroup=name,showlegend=False),
                          row=i+1, col=1)
            comps.append(name)
#    # Plot vertical line for every component
#    for n in data[i]['cmp'].keys():
#        #header, atom = info['header'], info['atom']
#        vobs = data[i]['cmp'][n]['vobs']
#        if info['pubstyle']:
#            plt.plot([vobs,vobs],[1.1,1.2],color='red',lw=0.5)
#        else:
#            z = data[i]['cmp'][n]['z']
#            pos = 1.08 if data[i]['cmp'][n]['val']%2==0 else 1.25
#            category =  data[i]['cmp'][n]['category']
#            plt.axvline(x=vobs,ls='dashed',color=colors[category],lw=.5,zorder=1,alpha=0.7)
#            if 'label' in data[i]['cmp'][n].keys():
#                label =  data[i]['cmp'][n]['label']
#                t = plt.text(vobs,pos,label,color=colors[category],weight='bold',fontsize=7,horizontalalignment='center')
#                t.set_bbox(dict(color='white', alpha=0.5, edgecolor=None))
    # Plot high-resolution co-added profile
    if info['details']:
        fig.add_trace(go.Scatter(**data[i]['prf'],**settings['prf']['feat'],line=dict(**settings['prf']['line']),
                                 customdata=get_custom_data(info['zmid'],data[i]['wav'],**data[i]['prf']),
                                 hovertemplate='<b>Co-added profile</b><br>'+text_coord,mode='lines',
                                 showlegend=False),
                      row=i+1, col=1)
    return fig, comps

def make_titles(data, height, header, dvmin, dvmax, table1, comment, **kwargs):
    titles = []
    vcent = (dvmin+dvmax)/2
    vhalf = (dvmax-dvmin)/2
    for i in range(len(table1)):
        text = '<b>%s %.2f</b>'%(header[i,0],float(header[i,1]))
        text += ' | f = %.6f'%float(header[i,2])
        if data[i]['shift']!=0:
            text += ' | >> %.5f km/s'%(-data[i]['shift'])
        if data[i]['cont']!=0:
            text += ' | <> %.5f'%data[i]['cont']
        if data[i]['zero']!=0:
            text += ' | __ %.5f'%(-data[i]['zero'])
        text += ' | npix = %s'%header[i,-2]
        text += ' | ndf = %s'%header[i,-1]
        text += ' | &#935;<sup>2</sup><sub>abs</sub> = %s'%header[i,-4]
        text += ' | &#935;<sup>2</sup><sub>red</sub> = %s'%header[i,-3]
        if header[i,4]!='0':
            text +=' | q = %.0f'%float(header[i,4])
        if 'overlap' in comment[i,0]:
            text += ' | <b>Overlapping system:</b> %s'%comment[i,1]
        if 'external' in comment[i,0]:
            text += ' | <b>External system:</b> %s'%comment[i,1]
        titles.append(text)
    return titles
