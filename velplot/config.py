import os
import yaml

def default_settings(pubstyle=False,mode=None,**kwargs):
    config = {}
    # Change linewidths for interactive mode
    if mode=='interactive':
        config['settings'] = {'res':{'line':{'width':1.0,'color':'grey'},
                                     'feat':{'opacity':1.0,'line_shape':'vh'}},
                              'err':{'line':{'width':2.0,'color':'cyan'},
                                     'feat':{'opacity':1.0,'line_shape':'vh'}},
                              'mod':{'line':{'width':10.0,'color':'yellow'},
                                     'feat':{'opacity':0.5,'line_shape':'linear'}},
                              'dat':{'line':{'width':1.0,'color':'black'},
                                     'feat':{'opacity':1.0,'line_shape':'vh'}},
                              'cmp':{'line':{'width':1.0,'color':'orange'},
                                     'feat':{'opacity':0.7,'line_shape':'linear'}},
                              'prf':{'line':{'width':1,'color':'orangered'},
                                     'feat':{'opacity':1.0,'line_shape':'linear'}}
                   }
    else:
        config['settings'] = {'res':{'lw':0.4,'alpha':1.0,'drawstyle':'steps','color':'magenta'},
                              'err':{'lw':0.2,'alpha':1.0,'drawstyle':'steps','color':'cyan'},
                              'mod':{'lw':1.0,'alpha':0.8,'drawstyle':'default','color':'lime'},
                              'dat':{'lw':0.3,'alpha':1.0,'drawstyle':'steps','color':'black'},
                              'cmp':{'lw':0.2,'alpha':1.0,'drawstyle':'default','color':'orange'},
                              'prf':{'lw':1.0,'alpha':0.5,'drawstyle':'default','color':'orange'}
                              }
    # Adapt style if image for publication
    config['settings']['ylimit'] = [-0.6,2.1]
    if pubstyle:
        config['settings']['res']['color'] = 'black'
        config['settings']['mod']['color'] = 'red'
        config['settings']['mod']['lw'] = 1
        config['settings']['ylimit'] = [-0.35,1.9]
    # Color of individual component
    config['settings']['colors'] = {'sameion':'orange',
                                    'otherion':'green',
                                    'interloper':'blue',
                                    'external':'red'}
    config['settings']['label'] = {'size':15}
    config['settings']['fill'] = False
    return config

def load_config(fortfile,**kwargs):
    # Update setting with custom file
    if fortfile.endswith('.yaml') or fortfile.endswith('.yml'):
        with open(fortfile) as f:
            config = yaml.load(f, Loader=yaml.FullLoader)
        assert config!=None, 'Empty configuration file. Abort.'
        config = {**kwargs,**config}
    else:
        config = {**kwargs}
        config['fortfile'] = fortfile
    settings = default_settings(**config)
    config = merge(config,settings)
    config['current_dir'] = os.getcwd()
    if os.path.exists(config['fortfile']):
        config['path'] = os.path.dirname(os.path.abspath(config['fortfile']))
    config['fortfile'] = config['fortfile'].split('/')[-1]  
    return config

def merge(source, destination):
    """
    run me with nosetests --with-doctest file.py

    >>> a = { 'first' : { 'all_rows' : { 'pass' : 'dog', 'number' : '1' } } }
    >>> b = { 'first' : { 'all_rows' : { 'fail' : 'cat', 'number' : '5' } } }
    >>> merge(b, a) == { 'first' : { 'all_rows' : { 'pass' : 'dog', 'fail' : 'cat', 'number' : '5' } } }
    True

    References
    ----------
    https://stackoverflow.com/a/20666342/8626695
    """
    for key, value in source.items():
        if isinstance(value, dict):
            # get node or create one
            node = destination.setdefault(key, {})
            merge(value, node)
        else:
            destination[key] = value
    return destination
