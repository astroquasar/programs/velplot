# System
import os

# External
from datetime import datetime

# Local
from .fit import get_data
from .build import store_data
from .plot import make_pdf, custom_plot, interplot
from .config import load_config

def make_figure(fortfile,atomdir='./atom.dat',details=False,dispersion=0.01,dv=None,error=False,
                extra=None,fit=False,getwave=False,header=None,illcond=False,nores=False,
                output=(datetime.now()).strftime('%y%m%d-%H%M%S'),mode='standard',unscale=False,
                vpfit='vpfit',vpsetup='./vp_setup.dat',zmid=None,cont=False,ncols=None,
                nrows=None,posidx=[],pubstyle=False,seaborn=True,display_ratio=100,
                save_to_file=False):
    '''
    Main function to create velocity plot.

    Parameters
    ----------
    fortfile : str
      fort.13 file to be read
    atomdir : str
      Path to a custom atom.dat
    details : bool
      Plot individual Voigt profiles
    dispersion : float
      Spectral velocity dispersion for high-resolution individual Voigt profiles
    dv : float
      Custom velocity range for plotting
    error : bool
      Run VPFIT only once to get error estimates
    extra : str
      Add vertical line to identify specific region. Multiple velocities
      can be specified by using the colon symbol (:) to separate the values.
    fit : bool
      Run VPFIT and embed final results in fort.13
    getwave : bool
      Get wavelength array from the spectrum
    header : str
      List of transition IDs of each fitting region
    illcond : bool
      Run VPFIT with ill-conditioning
    nores : bool
      Do no plot the residuals
    output : str
      Give custom output filename
    save2pdf : bool
      Save figure to PDF file
    unscale : bool
      Don't correct the data and distort the models
    vpfit : str
      Path to a custom vpfit executable
    vpsetup : str
      Path to a custom vp_setup.dat
    zmid : float
      Central redshift to plot transitions

    Examples
    --------
    As executables:

    >>> velplot fort.13 --output turbulent --vpfit vpfit10 --header header.dat

    As Python script:

    >>> import velplot
    >>> velplot.make_figure(fortfile='fort.13',header='header.dat',details=True,getwave=True)
    '''
    # Configuration setup
    config = load_config(**locals())
    # Move to local repository
    os.chdir(config['path'])
    # Execute VPFIT
    info = get_data(**config)
    # Retrive data and build models
    data = store_data(**info)
    # Plot everything
    if config['mode']=='standard':
        make_pdf(info,data,**config)
    elif config['mode']=='custom':
        custom_plot(info,data,**config)
    elif config['mode']=='interactive':
        interplot(info,data,**config)
    # Delete chunks and exit
    os.system('rm -rf chunks/')
    os.chdir(config['current_dir'])

