# System
import sys

# External
import numpy
from matplotlib import re
from scipy.ndimage import gaussian_filter1d

# Local
from ..constants import *
from .voigt import voigt_model
from .readspec import read_spec

def store_data(table1,table2,header,atom,comment,dvmin,dvmax,zmid,dispersion=0.01,daoaun=0,
               nores=False,unscale=False,getwave=False,details=False,pubstyle=False,mode=None,
               **kwargs):
    '''
    Plot system and model.
    
    Parameters
    ----------
    i : int
      Index of the fitting region.
    daoaun : float
      Unit of da/a value in VPFIT.
    nores : bool
      Flag to not plot residuals
    unscale : bool
      Flag to not correct the data and distort the models
    getwave : bool
      Flag to get wavelength array from the spectrum
    details : bool
      Flag to plot individual Voigt profiles
    '''
    if mode=='interactive':
        dispersion = 1
    data = {}
    for i in range(len(table1)):
        print(header[i,0],header[i,1],table1[i][0],'chunks/vpfit_chunk'+'%03d'%(i+1))
        # Extract region information
        data[i] = check_shift(**locals())
        data[i]['ion'] = header[i][0]
        data[i]['wav'] = header[i][1]
        # Load data and model
        chunk = numpy.loadtxt('chunks/vpfit_chunk'+'%03d'%(i+1)+'.txt',comments='!')
        ibeg  = abs(chunk[:,0]-table1[i][2]).argmin()+1 if table1[i][2]!=table1[i][3] else 0
        iend  = abs(chunk[:,0]-table1[i][3]).argmin()-1 if table1[i][2]!=table1[i][3] else -1
        wa    = chunk[ibeg:iend,0]
        fl    = chunk[ibeg:iend,1]
        er    = chunk[ibeg:iend,2]
        mo    = chunk[ibeg:iend,3]
        wamid = float(comment[i,2])
        pos   = abs(wa-wamid).argmin()
        vel   = 2*(wa-wamid)/(wa+wamid)*c
        # Estimate the half-pixel size to shift the steps of the flux array when plotting
        pos    = abs(wa-(table1[i][2]+table1[i][3])/2).argmin()
        pix1   = wa[pos]
        pix2   = (wa[pos]+wa[pos-1])/2
        dempix = 2*(pix1-pix2)/(pix1+pix2)*c
        # Plot residuals based on the flux, error, and model from the chunks
        if nores==False and table1[i][2]!=table1[i][3]:
            velo = vel if unscale else vel+data[i]['shift']
            res  = (fl-mo)/er/10+1.7
            data[i]['res'] = {'x':velo+dempix,'y':res}
        # Plot model corrected for floating zero and continuum and velocity shift
        if table1[i][2]!=table1[i][3]:
            corr   = data[i]['cont']+data[i]['slope']*(wa/((1+data[i]['loc'])*1215.6701)-1)
            model  = mo/corr+data[i]['zero']
            model  = model / (1+data[i]['zero'])
            model  = mo if unscale else model
            velo   = vel if unscale else vel+data[i]['shift']
            data[i]['mod'] = {'x':velo,'y':model}
            data[i]['err'] = {'x':velo+dempix,'y':er}
        # Plot data
        if getwave:
            specwa,specfl = read_spec(i,table1)
            wabeg   = wamid * (2*c+dvmin) / (2*c-dvmin)
            waend   = wamid * (2*c+dvmax) / (2*c-dvmax)
            ibeg    = abs(specwa-wabeg).argmin()
            iend    = abs(specwa-waend).argmin()
            ibeg    = ibeg-1 if ibeg>0 else ibeg
            iend    = iend+1 if iend<len(specwa)-1 else iend
            wa = specwa[ibeg:iend]
            fl = specfl[ibeg:iend]
            pos     = abs(wa-wamid).argmin()
            vel     = 2*(wa-wamid)/(wa+wamid)*c
        corr   = data[i]['cont']+data[i]['slope']*(wa/((1+data[i]['loc'])*1215.6701)-1)
        flux   = fl/corr + data[i]['zero']
        flux   = flux / (1+data[i]['zero'])
        flux   = fl if unscale else flux
        velo   = vel if unscale else vel+data[i]['shift']
        data[i]['dat'] = {'x':velo+dempix,'y':flux}
        # Prepare high dispersion wavelength array
        start  = wamid * (2*c+dvmin) / (2*c-dvmin)
        end    = wamid * (2*c+dvmax) / (2*c-dvmax)
        val    = 1
        wave   = [start] # [start-2]
        dv     = dispersion
        while wave[-1]<end: # while wave[-1]<end+2:
            wave.append(wave[-1]*(2*c+dv)/(2*c-dv))
        wave   = numpy.array(wave)
        vel    = 2*(wave-wamid)/(wave+wamid)*c
        vel    = vel-data[i]['shift'] if unscale else vel
        model  = [1]*len(vel)
        show   = []
        n      = 1
        data[i]['cmp'] = {}
        for k in range(len(table2)):
            complist = numpy.empty((0,2))
            z = float(re.compile(r'[^\d.-]+').sub('',table2[k][2]))
            N = float(re.compile(r'[^\d.-]+').sub('',table2[k][1]))
            b = float(re.compile(r'[^\d.-]+').sub('',table2[k][3]))
            ''' Plot high dispersion Voigt profiles, lines, and labels for each component '''
            for p in range(len(atom)):
                alpha = table2[k][-2]*daoaun
                cond1 = table2[k][0]==atom[p,0]
                cond2 = table2[k][0] not in ['<>','>>','__','<<']
                cond3 = wa[0] < (1+z)*float(atom[p,1]) < wa[-1]
                if cond1 and cond2 and cond3:
                    lambda0 = float(atom[p,1])
                    data[i]['cmp'][n] = {'z':z,'N':N,'b':b,'alpha':alpha,
                                         'ion':atom[p,0],'wav':atom[p,1],'osc':atom[p,2],
                                         'gam':atom[p,3],'pos':k+1}
                    if details:
                        q       = float(atom[p,5])
                        wavenum = 1./(lambda0*10**(-8))
                        wavenum = wavenum - q*(alpha**2-2*alpha)
                        lambda0 = 1/wavenum*10**(8)
                        f       = float(atom[p,2])
                        gamma   = float(atom[p,3])
                        profile = voigt_model(N,b,wave/(z+1),lambda0,gamma,f)
                        model   = model*profile
                        vsig    = table1[i][4]/dv
                        conv    = gaussian_filter1d(profile,vsig)
                        if unscale:
                            corr = data[i]['cont']+data[i]['slope']*(wave/((1+z)*1215.6701)-1)
                            conv = conv*corr-data[i]['zero']
                            conv = conv/(1-data[i]['zero'])
                        data[i]['cmp'][n]['data'] = {'x':vel,'y':conv}
                    #if atom[p,1]==header[i][1] or abs(float(atom[p,1])-float(header[i][1])) > 0.1:
                    lobs  = lambda0*(z+1)
                    vobs  = 2*(lobs-wamid)/(lobs+wamid)*c
                    vobs  = vobs - data[i]['shift'] if unscale else vobs
                    data[i]['cmp'][n]['vobs'] = vobs
                    data[i]['cmp'][n]['val'] = val
                    zdiff = abs(2*(z-zmid)/(z+zmid))
                    category = 'sameion' if zdiff < 0.003 and atom[p,0]==header[i][0] \
                        else 'otherion' if zdiff < 0.003\
                        else 'interloper' if zdiff > 0.003 and atom[p,0] in ['HI','??'] \
                        else 'external'
                    data[i]['cmp'][n]['category'] = category
                    lab1 = table2[k][-3]
                    lab2 = ''.join(re.findall('[a-zA-Z]+',table2[k][2][-2:]))
                    data[i]['cmp'][n]['tie'] = lab2
                    if table2[k][2][-1].isdigit()==True and str(lab1)+category not in show:
                        data[i]['cmp'][n]['label'] = lab1
                        show.append(str(lab1)+category)
                        val = val + 1
                    elif table2[k][2][-1].isdigit()==False and lab2+category not in show:
                        data[i]['cmp'][n]['label'] = lab2
                        show.append(lab2+category)
                        val = val + 1                         
                    n += 1
        if details:
            vsig  = table1[i][4]/dv
            model = gaussian_filter1d(model,vsig)
            if unscale:
                corr  = data[i]['cont']+data[i]['slope']*(wave/((1+z)*1215.6701)-1)
                model = model*corr-data[i]['zero']
                model = model/(1-data[i]['zero'])
            data[i]['prf'] = {'x':vel,'y':model}
    return data

def check_shift(i,dvmin,dvmax,table1,table2,header,comment,pubstyle=False,**kwargs):
    '''
    Check applied shifts, zero or continuum levels in input model.
    '''
    
    # Calculate velocity at center position of the plots
    left  = table1[i][2]
    right = table1[i][3]
    shift,cont,slope,loc,zero = 0,1,0,0,0
    for j in range(len(table2)):
        val = table2[j][0]
        N   = float(re.compile(r'[^\d.-]+').sub('',table2[j][1]))
        z   = float(re.compile(r'[^\d.-]+').sub('',table2[j][2]))
        b   = float(re.compile(r'[^\d.-]+').sub('',table2[j][3]))
        reg = table2[j][4]
        if val==">>" and (reg==i+1 or (reg==0 and left<1215.6701*(z+1)<right)):
            shift = -b
        if val=="<>" and (reg==i+1 or (reg==0 and left<1215.6701*(z+1)<right)):
            cont  = N
            slope = b
            loc   = z
        if val=="__" and (reg==i+1 or (reg==0 and left<1215.6701*(z+1)<right)):
            zero = -N
    return {'shift':shift,'cont':cont,'slope':slope,'loc':loc,'zero':zero}

