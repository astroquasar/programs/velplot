# System
import os
import shutil
from glob import glob

# External
import numpy

# Local
from .atominfo import atom_list
from .fortread import read_fort13,read_fort26,get_dv

def get_data(fortfile,posidx=[],header=[],atomdir='./atom.dat',vpsetup='./vp_setup.dat',
             vpfit='vpfit',fit=False,error=False,illcond=False,dv=None,zmid=None,cont=False,
             pubstyle=False,details=False,**kwargs):
    # Get full path to VPFIT dependencies
    if type(header)==str:
        header = numpy.loadtxt(os.path.abspath(header),dtype='str',comments='!',delimiter='\n',ndmin=1)
    if atomdir!='./atom.dat': atomdir = os.path.abspath(atomdir)
    if vpsetup!='./vp_setup.dat': vpsetup = os.path.abspath(vpsetup)
    vpfit = os.path.abspath(vpfit) if os.path.exists(vpfit) else shutil.which(vpfit)
    # Prepare setup files
    atom,pcvals,lastchtied,daoaun = make_setup(atomdir,vpsetup)
    # Perform fitting is requested
    fit_fort(fortfile,fit,error,illcond,pcvals,vpfit)
    # Extract model details
    if fortfile.split('.')[-1]=='13' or fortfile.split('.')[0]=='f13':
        header,comment,table1,table2 = read_fort13(fortfile,lastchtied,atom,header)
    if fortfile.split('.')[-1]=='26' or fortfile.split('.')[0]=='f26':
        header,comment,table1,table2 = read_fort26(fortfile,lastchtied,atom,header)
    dvmin,dvmax,zmid = get_dv(header,table1,comment,atom,dv,zmid)
    header = create_chunks(fortfile,table1,header,pcvals,vpfit,cont)
    return {**locals(),**kwargs}

def make_setup(atomdir='./atom.dat',vpsetup='./vp_setup.dat'):
    # Check if both atom.dat and vp_setup.dat are found
    assert os.path.exists(os.path.abspath(atomdir)), 'atom.dat not found at %s'%os.path.abspath(atomdir)
    assert os.path.exists(os.path.abspath(vpsetup)), 'atom.dat not found at %s'%os.path.abspath(vpsetup)
    # Set up atomic transition list
    atom = atom_list(atomdir)
    os.environ['ATOMDIR'] = os.path.abspath(atomdir)
    # Set up VPFIT settings
    os.environ['VPFSETUP'] = os.path.abspath(vpsetup)
    assert os.path.exists(vpsetup), 'ERROR: vp_setup.dat not found...'
    pcvals,lastchtied,daoaun = False,'z',1
    for line in numpy.loadtxt(vpsetup,dtype='str',delimiter='\n'):
        if 'pcvals' in line and line.split()[0][0]!='!':
            pcvals = True
        if 'lastchtied' in line and line.split()[0][0]!='!':
            lastchtied = line.split()[1].lower()
        if 'daoaun' in line and line.split()[0][0]!='!':
            daoaun = float(line.split()[1])
    # Remove previously created chunks
    if os.path.exists('./chunks/')==True:
        os.system('rm -rf chunks/')
    return atom,pcvals,lastchtied,daoaun

def create_chunks(fortfile,table1,header,pcvals,vpfit,cont=False):
    '''
    Generate data chunk for each fitted region.

    Parameters
    ----------
    fortfile : str
      Path to input fort.13 or fort.26 file.
    pcvals : bool
      Flag to check if pcvals command in vp_setup.dat
    vpfit : str
      VPFIT executable filename
    '''
    opfile = open('fitcommands','w')
    opfile.write('d\n')                 # Run display command + enter
    if pcvals:                          # If development tools called...
        opfile.write('\n')              # ...used default setup -> enter only
    opfile.write('\n')                  # Used default selfeter (logN) -> enter only
    opfile.write(fortfile+'\n')         # Insert fort file name + enter
    for line in table1:
        if cont and '.fits' in line[0]:
            opfile.write('\n')
    opfile.write('\n')                  # Plot the fitting region (default is yes) -> enter only
    if len(table1)>1:           # If more than one transitions...
        opfile.write('\n')              # ...select first transition to start with (default)
    opfile.write('as\n\n\n')
    for line in table1:
        opfile.write('\n\n\n\n')
    #if len(table1)>1:           # If more than one transitions...
    #    opfile.write('\n')              # ...select first transition to start with (default)
    opfile.write('n\n\n')
    opfile.close()
    os.system(vpfit+' < fitcommands > termout')
    assert len(glob('vpfit_chunk*'))==len(table1), '\n\tThere is a problem creating the chunks. Not all chunks were generated. Try the following --cont argument and check that the path to data files in header is valid.\n'
    assert 'Zero wavelength range' not in open('termout').read(), '\n\tERROR: A fitting region has no available data! Check your spectrum and/or model.\n'
    output = numpy.loadtxt('termout',dtype='str',delimiter='\n')
    for i in range(len(output)):
        if 'Statistics for each region :' in output[i]:
            i,k = i+2,0
            while 'Plot?' not in output[i]:
                header[k,-4] = 'n/a' if '*' in output[i].split()[2] else '%.4f'%(float(output[i].split()[2]))
                header[k,-3] = 'n/a' if '*' in output[i].split()[2] else '%.4f'%(float(output[i].split()[2])/float(output[i].split()[4]))
                header[k,-2] = output[i].split()[3]
                header[k,-1] = output[i].split()[4]
                k = k + 1
                i = i + 2
    if os.path.exists('chunks')==False:
        os.system('mkdir chunks')
    os.system('mv vpfit_chunk* chunks/')
    os.system('rm fitcommands termout')
    return header

def fit_fort(fortfile,fit,error,illcond,pcvals,vpfit):
    '''
    Do Voigt fitting and update model.
    '''
    opfile = open('fitcommands','w')
    command = 'e' if error else 'f' if fit else 'd'
    opfile.write('%s\n'%command)    # Run fit command + enter
    if pcvals:                      # If development tools called...
        if illcond:
            opfile.write('il\n')
        opfile.write('\n')          # ...used default setup -> enter only
    opfile.write('\n')              # Used default parameter (logN) -> enter only
    opfile.write(fortfile+'\n')     # Insert fort file name + enter
    opfile.write('n\n')             # Do not plot the region + enter
    opfile.write('\n')              # Do not fit more line and exit VPFIT -> enter only
    opfile.close()
    os.system(vpfit+' < fitcommands > termout')
    if fit or illcond:
        ''' Read fort.13 and store header and first guesses '''
        i,flag,header,guesses = 0,0,[],[]
        line13 = [line.replace('\n','') for line in open(fortfile,'r')]
        while i < len(line13):
            if '*' in line13[i]:
                flag = flag+1
            if '*' not in line13[i] and flag==1:
                header.append(line13[i]+'\n')
            if '*' not in line13[i] and flag==2:            
                guesses.append(line13[i]+'\n')
            i = i + 1
        ''' Take results from fort.18 '''
        i,results = 0,[]
        line18 = numpy.loadtxt('fort.18',dtype='str',delimiter='\n')
        for i in range(len(line18)-1,0,-1):
            if 'chi-squared' in line18[i]:
                a = i + 2
                break
        for i in range(a,len(line18)):
            results.append(line18[i]+'\n')
            if len(line18[i])==1:
                break
        ''' Update fort.13 and embed results from fort.18 '''
        fort = open(fortfile,'w')
        for line in ['*\n']+header+['*\n']+results+guesses:
            fort.write(line)
        fort.close()
